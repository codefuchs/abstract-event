export default class AbstractEvent {
	static defaultArgs = {
		listeners	: [],
		events		: [],
	}

	constructor(args) {
		this._args = {
			...AbstractEvent.defaultArgs,
			...args,
		};

		this._listeners	= [];
		this._events	= [];

		this.listeners	= this._args.listeners;
		this.events		= this._args.events;
	}

	addListener(fn) {
		if (typeof fn != 'function') {
			throw new TypeError(`fn must be a function`);
		}

		this._listeners.push(fn);
		return fn;
	}
	addListenerOnce(fn) {
		const fnWrapper = (...args) => {
			this.removeListener(fnWrapper);
			return fn.apply(undefined, args);
		};
		this.addListener(fnWrapper);
		return fnWrapper;
	}
	removeListener(fn) {
		let indexListener = this._listeners.indexOf(fn);
		if (indexListener == -1) {
			return;
		}

		return this._listeners.splice(indexListener, 1);
	}
	get listeners() {
		return Array.from(this._args.listeners);
	}
	set listeners(fns) {
		if (!fns.every(fn => typeof fn == 'function')) {
			throw new TypeError('fns must be an array of functions');
		}

		this._listeners.filter(fn => !fns.includes(fn))
		.forEach(listener => this.removeListener(listener));

		fns.filter(fn => !this._listeners.includes(fn))
		.forEach(listener => this.addListener(listener));
	}

	addEvent(ev) {
		if (!(ev instanceof AbstractEvent)) {
			throw new TypeError('ev must be an instance of AbstractEvent');
		}

		// Event ist bereits aufgenommen worden
		else if (this._events.find(eventObject => eventObject.event == event)) {
			return false;
		}

		const eventObject = {
			event		: ev,
			listener	: args => ev.trigger(args),
		};
		this._events.push(eventObject);
		this.addListener(eventObject.listener);
	}
	removeEvent(ev) {
		if (!(ev instanceof AbstractEvent)) {
			throw new TypeError('ev must be an instance of AbstractEvent');
		}
		const indexEvent = this._events
			.findIndex(eventObject => eventObject.event == ev);

		// Event ist in dieser Instanz nicht registriert
		if (indexEvent == -1) {
			return false;
		}

		const eventObject = this._events[indexEvent];
		this.removeListener(eventObject.listener);
		this._events.splice(indexEvent, 1);
	}
	get events() {
		return this._args.events.map(eventObject => eventObject.event);
	}
	set events(evs) {
		if (!evs.every(ev => ev instanceof AbstractEvent)) {
			throw new TypeError('evs must be an instance of AbstractEvent');
		}

		const events		= this._events.map(ev => ev.event);
		events.filter(ev => !evs.includes(ev))
		.forEach(listener => this.removeListener(listener));

		evs.filter(ev => !events.includes(ev))
		.forEach(listener => this.addListener(listener));
	}

	async trigger(args) {
		for (let listener of this._listeners) {
			const interrupted = await listener(args);
			if (interrupted === false) {
				break;
			}
		}
	}
}
